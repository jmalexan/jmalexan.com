import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      animation: {
        blink: 'blink 1.5s steps(1) infinite',
      },
      keyframes: {
        blink: {
          '0%, 100%': { opacity: "0" },
          '50%': { opacity: "1" },
        },
      },
    },
  },
  plugins: [],
}
export default config
