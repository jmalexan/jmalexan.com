import { readFileSync, readdirSync } from "fs";
import matter from "gray-matter";

export default function PostList({ maxItems, andMore = false }: { maxItems?: number, andMore?: boolean }) {
    const posts = readdirSync('pages/posts');
    let fms = posts.map(post => {
        const { data } = matter(readFileSync(`pages/posts/${post}`, 'utf8'));
        return data;
    })
    fms = fms.filter(fm => fm.title && fm.published && fm.timePublished)
    fms.sort((a, b) => {
        return new Date(b.timePublished).getTime() - new Date(a.timePublished).getTime();
    })
    const totalPosts = fms.length;
    if (maxItems !== undefined) fms = fms.slice(0, maxItems)
    return (<>
        <ul className="list-disc list-inside">
            {fms.map(fm => {
                return (<li key={fm.title}>
                    <a href={`/blog/${fm.slug}`} className="hover:underline">{fm.title} <em>&#40;{new Date(fm.timePublished).toDateString()}&#41;</em></a>
                </li>)
            })}
            {andMore && totalPosts > 5 && <li><a href="/blog" className="hover:underline">and more...</a></li>}
        </ul>
    </>)
}