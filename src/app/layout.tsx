import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import { clsx } from 'clsx';
import './globals.css'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'jmalexan',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  const navLinks = [
    { href: "/about", label: "about" },
    { href: "/projects", label: "projects" },
    { href: "/blog", label: "blog" },
    { href: "mailto:me@jmalexan.com", label: "contact" },
  ]
  return (
    <html lang="en">
      <header>
        <link rel="me" href="https://mastodon.social/@NotDisliked" />
      </header>
      <body className={clsx(inter.className, "bg-black text-white")}>
        <nav className="bg-black sticky left-0 right-0 top-0 border-b border-white py-2">
          <div className="inline-block px-3 pb-1 min-[400px]:border-r min-[400px]:pb-0">
            <span>&gt;</span>
            <a href="/" className="hover:underline">jmalexan.com</a>
            <span className="animate-blink">_</span>
          </div>
          <div className="inline-flex px-3 gap-3">
            {navLinks.map(({ href, label }) => (
              <a href={href} key={href} className={clsx("hover:underline")}>{label}</a>
            ))}
          </div>
        </nav>
        <div className="p-5 w-[48rem] max-w-full">
          {children}
        </div>
      </body>
    </html>
  )
}
