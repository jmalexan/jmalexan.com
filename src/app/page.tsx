import PostList from "@/components/post-list";

export default function Home() {
  return (
    <main>
      <h1 className="text-4xl mb-3 font-bold">Hi there</h1>
      <p className="mb-3">My name&apos;s Jonathan Alexander, I&apos;m a software engineer</p>
      <p className="mb-3">I&apos;m currently at NVIDIA working on compiler verification.  I&apos;ve also built Where The Fuck is Xur and Bungie Pls, two sites centered on various aspects of the video game Destiny 2.  For more info on these or current and previous work experiences, take a look at the links on the top of the page.</p>
      <p className="mb-3">Below are my most recent blog posts.  I make no guarantees on the topics or quality of the writing.</p>
      <PostList maxItems={5} andMore={true} />
    </main>
  )
}
