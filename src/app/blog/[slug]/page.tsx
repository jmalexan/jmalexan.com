import { readFileSync, readdirSync } from "fs"
import { compileMDX } from 'next-mdx-remote/rsc'
import { notFound } from "next/navigation"
import matter from 'gray-matter';

export default async function Post({ params }: { params: { slug: string } }) {
    if (!params.slug) {
        return null
    }

    const postFile = findPostBySlug(params.slug);

    if (!postFile) {
        return notFound();
    }

    const { content, frontmatter } = await compileMDX<{ title: string, timePublished: string }>({
        source: readFileSync(`pages/posts/${postFile}`, 'utf8'),
        options: { parseFrontmatter: true },
    })
    return (<>
        {frontmatter.title && <h1 className="text-4xl mb-1 font-bold">{frontmatter.title}</h1>}
        {frontmatter.timePublished && <p className="mb-3"><em>{new Date(frontmatter.timePublished).toDateString()}</em></p>}
        <div className="markdown">
            {content}
        </div>
    </>
    )

}

export async function generateStaticParams() {
    const posts = readdirSync('pages/posts').map((fileName) => {
        return {
            slug: fileName.replace(/\.md$|\.mdx$/, ''),
        }
    })

    return posts.map((post) => ({
        slug: post.slug,
    }))
}

function findPostBySlug(slug: string) {
    const files = readdirSync('pages/posts');

    for (const file of files) {
        const content = readFileSync(`pages/posts/${file}`, 'utf8');
        const { data } = matter(content);

        if (data.slug === slug) {
            return file;
        }
    }

    return undefined;
}