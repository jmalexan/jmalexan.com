import PostList from "@/components/post-list";

export default function BlogIndex() {
    return (<>
        <h1 className="text-4xl mb-6 font-bold">Blog</h1>
        <p className="mb-3">Below are various thoughts on video games, Destiny 2, programming, books and movies, and random self serious thoughts on life.</p>
        <PostList />
    </>)
}