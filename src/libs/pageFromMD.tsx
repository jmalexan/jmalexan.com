import { compileMDX } from 'next-mdx-remote/rsc'
import { readFileSync } from "fs"

export default function pageFromMD(path: string) {
    return async () => {
        const { content } = await compileMDX({
            source: readFileSync(path, 'utf8'),
        })
        return (<div className="markdown">{content}</div>)
    }
}