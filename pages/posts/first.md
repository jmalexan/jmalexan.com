---
title: Hello World
slug: first
published: true
timePublished: 2024-01-04
---

Welcome to my new home on the internet.

I’ve been flirting with the idea of writing for a long while now.  I often find myself grappling with some thoughts or feelings in my head that resist understanding.  Trying to work through them in my head can often feel like my brain moves too fast to land on anything productive.  Lots of ideas, but very little insight.  Usually when I’m lamenting this problem, it occurs to me that writing the thoughts out might be a good solution to this.  Crafting a narrative and a progression out of something might slow my thoughts down enough to actually have a revelation for once.  But because that involves doing something new to me, I dodge the idea and keep on spinning in circles on the imaginary office chair in my head.  For me (and I’m sure many people), starting new things can be hard because the initial attempts can feel pointless.  What’s the point of doing something bad that only matters to me, when I could just dodge that and do something unrelated that I know I’m good at instead.

Two things happened that have brought me from not writing to what you’re reading right now.  The first was the new year.  I usually avoid making any kind of new year’s resolutions; my thinking is, if I wanted to do something I’d just start doing it regardless of the calendar.  It’s a common trope for people to pledge to transform their lives when the year ticks up, only to let their gym memberships expire before the season is over.  In this case though, I’ve been on the brink of writing for so long, all I needed was an excuse to start.  2024 is as good of an excuse as any.  The second thing is an understanding that I struggle to do things that are just for myself.  I like to interact with, and often help others in most things I do.  It was always going to be a struggle to start a journal because I don’t consider myself a valid audience.  I had a realization though: if I start a blog, even if I never make any effort advertise it or post it anywhere, the audience shifts from me to other people.  Even if the amount of other people reading is 0 it’s still not me I’m writing for, because someone else *could* see it at any given time.

While I’ve been fighting to finally put a cursor to textbox, as it turns out the social internet has been transforming as well.  After Twitter turned into a letter, I’ve been progressively uninstalling all the social media apps I can off my phone.  I’ve refocused on RSS feeds and a Mastodon account to follow a few technical creators and journalists that I care about.  And as I’ve read their works over the past year, I’ve been really taken by this idea of a return to a more scattered internet.  Running your own blogs, the world’s social accounts spread across instances and apps, all connected by one big federated network.  Maybe I can contribute to that vision myself, in a small way.

I probably won’t post all my writings here, I think starting to write at all has made me more comfortable with the idea of writing for myself in private.  But I do hope to write daily, as much as I can.

Here’s hoping this isn’t the last post you see from me here.
